﻿

namespace Quarto.Auth.Api.Models
{
    /// <summary>
    /// The state of the Response
    /// </summary>
    public enum ResponseState
    {
        /// <summary>
        /// Success response state
        /// </summary>
        Success,
        /// <summary>
        /// Error response state
        /// </summary>
        Error,
        /// <summary>
        /// Exception response state
        /// </summary>
        Exception
    }
}
